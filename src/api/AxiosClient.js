import axios from 'axios'
import { SERVICES } from '../settings';

export const BackendInstance = axios.create({
    baseURL: SERVICES.backend.baseUrl,
    headers: {
        'content-type':'application/json',
    }
});

export const JudgerInstance = axios.create({
  baseURL: SERVICES.judger.baseUrl,
  headers: {
      'content-type':'application/json',
  }
});

export const apiCall = (url, data, headers, method) => axios({
  method,
  url: url,
  data,
  headers
})

// Where you would set stuff like your 'Authorization' header, etc ...
// instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

// Also add/ configure interceptors && all the other cool stuff

// instance.interceptors.request...

BackendInstance.interceptors.request.use(
  config => {
    if (!config.headers.Authorization) {
      const userData = JSON.parse(localStorage.getItem('userData'));
      let token;
      if (userData && 'token' in userData) {
        token = userData.token;
        config.headers.Authorization = `Bearer ${token}`;
      }
    }
    return config;
  },
  error => Promise.reject(error)
);

JudgerInstance.interceptors.request.use(
  config => {
    if (!config.headers.Authorization) {
      const userData = JSON.parse(localStorage.getItem('userData'));
      let token;
      if (userData && 'token' in userData) {
        token = userData.token;
        config.headers.Authorization = `Bearer ${token}`;
      }
    }
    return config;
  },
  error => Promise.reject(error)
);
