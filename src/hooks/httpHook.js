import { useState, useCallback, useRef, useEffect } from 'react';
import { apiCall, BackendInstance, JudgerInstance } from '../api/AxiosClient';
import { BACKEND_SERVICE, JUDGER_SERVICE } from '../constants/services';


export const useHttpClient = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();

  // const activeHttpRequests = useRef([]);

  const getServiceInstance = service => {
    switch (service) {
      case BACKEND_SERVICE:
        return BackendInstance;
      case JUDGER_SERVICE:
        return JudgerInstance;
      default:
        return BACKEND_SERVICE;
    }
  }

  const sendRequest = useCallback(
    async (url, method = 'GET', body = null, service = BACKEND_SERVICE, headers = {}) => {
      setIsLoading(true);
      // const httpAbortCtrl = new AbortController();
      // activeHttpRequests.current.push(httpAbortCtrl);

      try {
        // const response = await fetch(url, {
        //   method,
        //   body,
        //   headers,
        //   signal: httpAbortCtrl.signal
        // });

        // const responseData = await response.json();

        // activeHttpRequests.current = activeHttpRequests.current.filter(
        //   reqCtrl => reqCtrl !== httpAbortCtrl
        // );
        method = String(method).toLowerCase();
        const instance = getServiceInstance(service);
        const { data: response } = await instance[method](url, body, headers);
        if (response.error) {
          throw new Error(response.message);
        }
        setIsLoading(false);
        return response;
      } catch (err) {
        setError(err.message);
        setIsLoading(false);
        throw err;
      }
    },
    []
  );

  const clearError = () => {
    setError(null);
  };

  useEffect(() => {
    return () => {
      // activeHttpRequests.current.forEach(abortCtrl => abortCtrl.abort());
    };
  }, []);

  return { isLoading, error, sendRequest, clearError };
};
