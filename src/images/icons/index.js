import { ReactComponent as ContestsSvg } from './contests.svg';
import { ReactComponent as LeaderboardSvg } from './leaderboard.svg';
import { ReactComponent as ProblemsSvg } from './problems.svg';
import { ReactComponent as StatusSvg } from './status.svg';
import { ReactComponent as AdminSvg } from './admin.svg';
import { ReactComponent as HelpSvg } from './help.svg';

export {
  ContestsSvg,
  LeaderboardSvg,
  ProblemsSvg,
  StatusSvg,
  AdminSvg,
  HelpSvg
}