import React, { Fragment } from 'react';

import './App.css';
import AppBar from './components/Navigation/AppBar';

import { Redirect, Switch, Route, Link, BrowserRouter as Router } from 'react-router-dom';
import Login from './components/Authentication/Login';
import ListProblems from './components/Problems/ListProblems';
import CreateProblem from './components/Problems/CreateProblem';
import MenuDrawer from './components/Navigation/MenuDrawer';
import Footer from './components/Footer/Footer';
import Signup from './components/Authentication/Signup';
import Home from './components/Home/Home';
import ShowProblem from './components/Problems/ShowProblem';
import { PrivateRoute } from './components/PrivateRoute';
import Admin from './components/Admin/Admin';
import { useAuth } from './hooks/authHook';
import { AuthContext } from './context/authContext';
import { makeStyles } from '@material-ui/core';
import AddTestCase from './components/Problems/AddTestCase';
import SubmitSolution from './components/Problems/SubmitSolution';
import Status from './components/Status/Status';


const useStyles = makeStyles((theme) => ({
  drawerWidth: {
    marginLeft: 0,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(7) + 1,
    }
  }
}));

const App = () => {
  const { token, login, logout, user } = useAuth();
  const classes = useStyles();
  let routes;
  if (token) {
    routes = (
      <Switch>
        <Route exact path="/home" component={Home} />
        <Route exact path="/">
          <Redirect to="/home"/>
        </Route>
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/status" component={Status} />
        <Route exact path="/problems" component={ListProblems} />
        <Route exact path="/problems/new" exact component={CreateProblem} />
        <Route exact path="/problems/submit/:id" exact component={SubmitSolution} />
        <Route exact path="/problems/show/:id" exact component={ShowProblem} />
        <Route exact path="/problems/edit/:id/testcases" exact component={AddTestCase} />
        <PrivateRoute exact path="/admin" component={Admin} />
        <Redirect to="/home" />
      </Switch>
    );
  } else {
    routes = (
      <Switch>
        <Route exact path="/home" component={Home} />
        <Route exact path="/">
          <Redirect to="/home"/>
        </Route>
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/status" component={Status} />
        <Route exact path="/problems" component={ListProblems} />
        <Route exact path="/problems/new" exact component={CreateProblem} />
        <Route exact path="/problems/submit/:id" exact component={SubmitSolution} />
        <Route exact path="/problems/show/:id" exact component={ShowProblem} />
        <Route exact path="/problems/edit/:id/testcases" exact component={AddTestCase} />
        <PrivateRoute exact path="/admin" component={Admin} />
        <Redirect to="/home" />
      </Switch>
    );
  }
  return (
    <AuthContext.Provider
      value={{
        isLoggedIn: !!token,
        token,
        user,
        login,
        logout
      }}
    >
      <Router>
        <AppBar/>
        <main className={classes.drawerWidth} >{ routes }</main>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
