import types from '../constants/actionTypes/authTypes';

const initialState = {
  user: null,
  token: null,
  error: null,
};

const authReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case types.REGISTER_SUCCESS:
      return {
        ...state,
        user: payload.user,
        token: payload.token
      }
    case types.REGISTER_FAILURE:
      return {
        ...state,
        error: payload.error
      };
    case types.LOG_IN_SUCCESS:
      console.log('reducer:', payload);
      return {
        ...state,
        user: payload.user,
        token: payload.token
      }
    case types.LOG_IN_FAILURE:
      return {
        ...state,
        error: payload.error
      };
    case types.LOG_OUT:
      return initialState;
    default:
      return state;
  }
};

export default authReducer;
