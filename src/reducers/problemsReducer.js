import types from '../constants/actionTypes/problemsTypes';

const initialState = {
  problems: [],
  error: null,
};

const problemReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case types.GET_PROBLEMS_SUCCESS:
      return {
        ...state,
        problems: payload.problems
      }
    case types.GET_PROBLEMS_FAILURE:
      return {
        ...state,
        error: payload.error
      };
    default:
      return state;
  }
};

export default problemReducer;