import { CircularProgress, createMuiTheme, MuiThemeProvider, Typography } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import { tableDark } from "../../../constants/themes";

let theme = createMuiTheme(tableDark);

const CustomTable = (props) => {
  const { isLoading, title, data, columns, options } = props;

  return (
    <MuiThemeProvider theme={theme}>
    <MUIDataTable
      title={
        <Typography variant="h6">
          { title }
        {isLoading && <CircularProgress size={24} style={{marginLeft: 15, position: 'relative', top: 4}} />}
        </Typography>
      }
      data={data}
      columns={columns}
      options={options}
    />
  </MuiThemeProvider>
  );
}
