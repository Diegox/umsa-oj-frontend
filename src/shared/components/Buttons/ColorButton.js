import React from 'react';
import { Button, withStyles } from "@material-ui/core";
import * as colors from "@material-ui/core/colors";


const ColorButton = (props) => {
  let { color, contrast, children, ...other } = props;
  if (!color) {
    color = 'green';
  }
  if (!contrast) {
    contrast = 500;
  }
  const CustomButton = withStyles(theme => {
    return {
    root: {
      color: theme.palette.getContrastText(colors[color][contrast]),
      backgroundColor: colors[color][contrast],
      '&:hover': {
        backgroundColor: colors[color][contrast + 100],
      },
      }
    }
  })(Button);
  return (
    <CustomButton {...other}>
      { children }
    </CustomButton>
  );
}

export default ColorButton;
