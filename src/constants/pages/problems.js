
export const Problem = {
    id: {
        label: "ID",
        name: 'problemId'
    },
    title: {
        label: 'Title',
        name: 'problemTitle'
    },
    author: {
        label: 'Author',
        name: 'problemAuthor'
    }
}
