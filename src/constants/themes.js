// export const mainDark = {
//   palette: {
//     type: 'dark',
//     primary: {
//       main: '#37474F'
//     },
//     secondary: {
//       main: '#FDD835'
//     }
//   }
// };

export const mainDark = {
  palette: {
    type: "dark",
    primary: {
      main: '#3a4750'
    },
    secondary: {
      main: '#FDD835'
    },
    background: {
      default: '#171b1f',
      paper: '#22272d'
    },
    action: {

    }
  }
};

export const mainDark2 = {
  palette: {
    type: "dark",
    primary: {
      main: '#00adb5'
    },
    secondary: {
      main: '#eeeeee'
    },
    background: {
      default: '#303841',
      paper: '#3a4750'
    },
    action: {

    }
  }
};



export const mainLight = {
  palette: {
    type: 'light',
    primary: {
      main: '#3C3B6E'
    },
    secondary: {
      main: '#B42033'
    },
    background: {
      default: '#CECECE',
      paper: '#FEFEFE'
    },
  }
};

export const tableDark = {
  palette: {
    type: 'dark',
    primary: {
      main: '#FDD835'
    },
    secondary: {
      main: '#3a4750'
    },
    background: {
      default: '#171b1f',
      paper: '#22272d'
    },
    action: {

    }
  },
  overrides: {
    MuiTableCell: {
      head: {
      }
    }
  }
};

export const tableLight = {
  palette: {
    type: 'light',
    primary: {
      main: '#FDD835'
    },
    secondary: {
      main: '#00c853'
    }
  },
  overrides: {
    MuiTableCell: {
      head: {
        fontSize: '17px',
        fontWeight: "bold"
      }
    }
  }
};
