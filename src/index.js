import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { MuiThemeProvider, createMuiTheme, CssBaseline, responsiveFontSizes } from '@material-ui/core';
import { BrowserRouter as Router } from 'react-router-dom';

import { Provider } from 'react-redux'

import store from './store';
import { mainDark, themeX } from './constants/themes';

let theme = createMuiTheme(mainDark);

theme = responsiveFontSizes(theme);


ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <Router>
        <CssBaseline/>
        <App />
      </Router>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
