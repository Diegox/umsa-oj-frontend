import React from 'react';
import PropTypes from 'prop-types';
import { createMuiTheme, makeStyles, MuiThemeProvider } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MUIDataTable from "mui-datatables";
import { Problem } from '../../constants/pages/problems';
import { tableDark, mainDark } from '../../constants/themes';
import Add from '@material-ui/icons/Add';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';

import { Link } from 'react-router-dom';
import ColorButton from '../../shared/components/Buttons/ColorButton';
import { IconButton } from '@material-ui/core';
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    // backgroundColor: theme.palette.background.paper,
  },
}));

const getProblemsAdmin = () => {
  const columns = [
    {
      name: Problem.id.name,
      label: Problem.id.label,
      options: {
        filter: true,
        sort: true,
      }
    },
    {
      name: Problem.title.name,
      label: Problem.title.label,
      options: {
        filter: true,
        sort: true,
      }
    },
    {
      name: Problem.author.name,
      label: Problem.author.label,
      options: {
        filter: true,
        sort: true,
      }
    },
    {
      name: 'actions',
      label: 'Actions',
      options: {
        filter: false,
        sort: false,
        empty: true.valueOf,
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <IconButton aria-label="delete"><Delete /></IconButton>
              <IconButton aria-label="edit"><Edit /></IconButton>
            </div>
          );
        }
      }
    }
  ];

  const data = [
    {
      problemId: '111',
      problemTitle: 'Diego',
      problemAuthor: 'Pepe',
      actions: 'asd '
    },
    {
      problemId: '111',
      problemTitle: 'Diego',
      problemAuthor: 'Pepe'
    },
    {
      problemId: '111',
      problemTitle: 'Diego',
      problemAuthor: 'Pepe'
    },
    {
      problemId: '111',
      problemTitle: 'Diego',
      problemAuthor: 'Pepe'
    },
    {
      problemId: '111',
      problemTitle: 'Diego',
      problemAuthor: 'Pepe'
    },
    {
      problemId: '111',
      problemTitle: 'Diego',
      problemAuthor: 'Pepe'
    },
    {
      problemId: '111',
      problemTitle: 'Diego',
      problemAuthor: 'Pepe'
    },
    {
      problemId: '111',
      problemTitle: 'Diego',
      problemAuthor: 'Pepe',
      actions: 'asd'
    }
  ];

  const options = {
    serverSide: true,
    filter: false,
    download: false,
    caseSensitive: false,
    // jumpToPage: true,
    print: false,
    // selectableRowsOnClick: true,
    // selectableRows: 'none',
    // filterType: 'dropdown',
    responsive: 'standard',
    // setRowProps: (row, dataIndex, rowIndex) => {
    //   return ({
    //     style: { background: rowIndex % 7 == 0 ? theme.palette.error.dark: rowIndex % 3 === 0 ? theme.palette.success.dark : null }
    //   });
    // },
    rowsPerPageOptions: [20, 50, 100],
    // sortOrder,
    searchPlaceholder: 'ID, Title, Author',
  };
  let theme = createMuiTheme(tableDark);

  return (
    <MuiThemeProvider theme={theme}>
        <ColorButton color="green" contrast={700} variant="contained" startIcon={<Add />} component={Link} to="/problems/new">
          Create Problem
        </ColorButton>
      <MUIDataTable
        title="Problems"
        data={data}
        columns={columns}
        options={options}
      />
    </MuiThemeProvider>
  );
}

export default function ScrollableTabsButtonAuto() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  let theme = createMuiTheme(mainDark);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <MuiThemeProvider theme={theme}>
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="secondary"
          textColor="secondary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          <Tab label="Problems" {...a11yProps(0)} />
          <Tab label="Contests" {...a11yProps(1)} />
          <Tab label="Users" {...a11yProps(2)} />
          <Tab label="Settings" {...a11yProps(3)} />
          <Tab label="Aea" {...a11yProps(4)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        { getProblemsAdmin() }
      </TabPanel>
      <TabPanel value={value} index={1}>
        Item Two
      </TabPanel>
      <TabPanel value={value} index={2}>
        Item Three
      </TabPanel>
      <TabPanel value={value} index={3}>
        Item Four
      </TabPanel>
      <TabPanel value={value} index={4}>
        Item Five
      </TabPanel>
    </div>
    </MuiThemeProvider>
  );
}
