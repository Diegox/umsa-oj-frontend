import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import MUIDataTable from "mui-datatables";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { createMuiTheme, makeStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { Button, Container } from "@material-ui/core";
import clsx from 'clsx';
import { tableDark } from "../../constants/themes";
import { useHttpClient } from "../../hooks/httpHook";
import moment from "moment";

const useStyles = makeStyles(
  theme => ({
    accepted: {
      color: '#4BB543',
    },
    wrongAnswer: {
      color: '#F32013',
    },
    boldFont: {
    }
  })
);

let theme = createMuiTheme(tableDark);

export default function Status() {
  const classes = useStyles();

  const initialState = {
    page: 0,
    rowsPerPage: 20,
    searchText: '',
    count: 0,
    data: [],
  };

  const { sendRequest, error, clearError, isLoading } = useHttpClient();

  const [tableState, setTableState] = useState(initialState);
  const { page, rowsPerPage, count, data, searchText } = tableState;

  useEffect(() => {
    getSubmissions(page, rowsPerPage, searchText);
  }, []);

  const columns = [
    {
      label: 'Run ID',
      name: 'id',
      options: {
        setCellHeaderProps: () => ({
          style: {
            fontWeight: 'bold'
          }
        }),
        sort: false
      }
    },
    {
      label: 'User',
      name: 'username',
      options: {
        setCellHeaderProps: () => ({
          style: {
            fontWeight: 'bold'
          }
        }),
        sort: false
      }
    },
    {
      name: 'veredict',
      label: "Result",
      options: {
        setCellHeaderProps: () => ({
          style: {
            fontWeight: 'bold'
          }
        }),
        setCellProps: value => {
          return {
            className: clsx({
              [classes.accepted]: value === 'Accepted',
              [classes.wrongAnswer]: value !== 'Accepted',
            }),
            style: {
              fontWeight: 'bold'
            }
          };
        },
        sort: false
      }
    },
    {
      label: "Memory",
      name: 'memory',
      options: {
        setCellHeaderProps: () => ({
          style: {
            fontWeight: 'bold'
          }
        }),
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return '-';
        }
      }
    },
    {
      label: "Time (in ms)",
      name: 'runtime',
      options: {
        setCellHeaderProps: () => ({
          style: {
            fontWeight: 'bold'
          }
        }),
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return  value >= 0 ? value : '-';
        }
      }
    },
    {
      label: "Language",
      name: 'language',
      options: {
        setCellHeaderProps: () => ({
          style: {
            fontWeight: 'bold'
          }
        }),
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          switch (value) {
            case 'java':
                return 'Java'
            case 'cpp':
                return 'C++'
            case 'python':
                return 'Python'
            default:
              break;
          }
        }
      }
    },
    {
      label: "Submition Time",
      name: 'timeSubmitted',
      options: {
        setCellHeaderProps: () => ({
          style: {
            fontWeight: 'bold'
          }
        }),
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return  moment(value).format("LLL");
        }
      }
    },

  ];

  const options = {
    filter: true,
    filterType: "dropdown",
    responsive:'standard',
    download: false,
    filter: false,
    print: false,
    serverSide: true,
    caseSensitive: false,
    pagination: true,
    selectableRows: 'none',
    responsive: 'standard',
    count,
    rowsPerPage,
    rowsPerPageOptions: [20, 50, 100],
    searchPlaceholder: 'Search for Run ID, User, Result, Language...',
    onChangePage: (currentPage) => {
      console.log('currentPage:', currentPage);
      getSubmissions(currentPage, rowsPerPage, searchText);
    },
    onSearchChange: (text) => {
      console.log('searchText:', text);
      getSubmissions(page, rowsPerPage, text);
    },
    onChangeRowsPerPage: (numberOfRows) => {
      console.log('numberOfRows:', numberOfRows);
      getSubmissions(page, numberOfRows, searchText);
    },
    onRowClick: (rowData) => {

    }
  };

  const getSubmissions = async (page, rowsPerPage, searchText) => {
    const query = `?page=${page}&rowsPerPage=${rowsPerPage}&searchText=${searchText}`;
    const { data: submissions } = await sendRequest('/submissions' + query);
    setTableState(prevState => ({
      ...prevState,
      data: submissions.data,
      count: submissions.count,
      rowsPerPage
    }));
  }

  return (
    <Container>
      <MuiThemeProvider theme={theme}>
        <MUIDataTable
          title={<h1 style={{ borderBottom: '2px solid #FDD835', display: 'inline' }}>Status</h1>}
          data={data}
          columns={columns}
          options={options}
        />
      </MuiThemeProvider>
    </Container>
  );
}
