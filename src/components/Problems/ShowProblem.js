import { Box, Button, Container, Grid, makeStyles, Typography, withStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useHttpClient } from '../../hooks/httpHook';
import parse from 'html-react-parser';
import ColorButton from '../../shared/components/Buttons/ColorButton';
import { Send, Add, List } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  actionButton: {
    margin: theme.spacing(1)
  }
}));

const ShowProblem = (props) => {
  const { history } = props;
  const classes = useStyles();
  const id = props.match.params.id;
  const { sendRequest, isLoading, error, clearError } = useHttpClient();

  const [problem, setProblem] = useState({});

  useEffect(() => {
    const getProblem = async (id) => {
      const { data: problem } = await sendRequest('/problems/' + id);
      console.log('problem: ', problem);
      setProblem(problem);
    }
    getProblem(id);
  }, []);

  const handleAddTestCase = () => {
    history.push(`/problems/edit/${id}/testcases`);
  }

  const handleSubmitSolution = () => {
    history.push(`/problems/submit/${id}`);
  }

  return (
    <Container>
      <Grid container spacing={2} direction="column">
        <Typography variant="h2" align="center">
          {problem.title}
        </Typography>

        <Typography variant="subtitle1" align="center">
          Time Limit: {problem.timeLimit}s
        </Typography>
        <Typography variant="subtitle1" align="center">
          Memory Limit: {problem.memoryLimit} MB
        </Typography>
        <Grid container direction="row" justify="center" alignItems="center">
          <ColorButton onClick={handleSubmitSolution} size="small" color="indigo" endIcon={<Send />} className={classes.actionButton}>
            Submit
          </ColorButton>
          <ColorButton onClick={handleAddTestCase} size="small" color="green" contrast={700} startIcon={<Add />} className={classes.actionButton}>
            Add Test Cases
          </ColorButton>
          <ColorButton size="small" color="yellow" contrast={700} endIcon={<List />} className={classes.actionButton}>
            Status
          </ColorButton>
        </Grid>
        <Typography variant="body1">
          {problem.statement && parse(problem.statement)}
        </Typography>

        <Typography variant="h5">
          <Box fontWeight="fontWeightBold" m={1}>
            Input description:
        </Box>
        </Typography>
        <div variant="body1">
          {problem.inputDescription && parse(problem.inputDescription)}
        </div>

        <Typography variant="h5">
          <Box fontWeight="fontWeightBold" m={1}>
            Output description:
          </Box>
        </Typography>
        <div variant="body1">
          {problem.outputDescription && parse(problem.outputDescription)}
        </div>
        <Typography variant="h5">
          <Box fontWeight="fontWeightBold" m={1}>
            Input example:
          </Box>
        </Typography>
        <Typography variant="body1">
          <pre>
            {problem.inputExample}
          </pre>
        </Typography>

        <Typography variant="h5">
        <Box fontWeight="fontWeightBold" m={1}>
            Output example:
          </Box>
        </Typography>
        <Typography variant="body1">
          <pre>
            {problem.outputExample}
          </pre>
        </Typography>

        <Typography variant="h5">
          <Box fontWeight="fontWeightBold" m={1}>
            Hints:
          </Box>
        </Typography>
        <Typography variant="body1">
          {problem.hints}
        </Typography>

        <Typography variant="h5">
          <Box fontWeight="fontWeightBold" m={1}>
            Author:
          </Box>
        </Typography>
        <Typography variant="body1">
          {problem.author}
        </Typography>
      </Grid>
    </Container>
  );
}

export default ShowProblem;
