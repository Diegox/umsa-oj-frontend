import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { Container, createMuiTheme, MuiThemeProvider, Grid, TextField, StepButton, Icon } from '@material-ui/core';
import { Editor } from '@tinymce/tinymce-react';
import { Send } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import ColorButton from '../../shared/components/Buttons/ColorButton';
import { AuthContext } from '../../context/authContext';
import { useHttpClient } from '../../hooks/httpHook';
import HTMLEditor from './HTMLEditor';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    paddingBottom: theme.spacing(2)
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
  textFields: {
    padding: '2%'
  },
  title: {
    borderBottom: '1px solid #FDD835',
    padding: '1%'
  },
  completed: {
    display: 'inline-block',
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#FDD835'
    },
    secondary: {
      main: '#00c853'
    }
  }
});

const handleEditorChange = (content, editor) => {
  console.log('editor: ', editor);
  console.log('Content was updated:', content);

}
const getSteps = () => {
  return ['Title and Limits', 'Statement', 'Input', 'Input example', 'Output', 'Output example', 'Author and Hints'];
}

const TitleAndLimits = (props) => {
  const classes = useStyles();
  return (
    <Grid container>
      <Grid item md={4} className={classes.textFields}>
        <TextField id="title" name="title" label="Title" variant="outlined" value={props.title} onChange={props.handleChange} fullWidth />
      </Grid>
      <Grid item md={4} className={classes.textFields}>
        <TextField id="time" name="timeLimit" type="number" label="Time Limit" helperText="meassured in seconds" variant="outlined" value={props.timeLimit} onChange={props.handleChange} fullWidth />
      </Grid>
      <Grid item md={4} className={classes.textFields}>
        <TextField id="memory" name="memoryLimit" type="number" label="Memory Limit" helperText="meassured in MBs" variant="outlined" value={props.memoryLimit} onChange={props.handleChange} fullWidth />
      </Grid>
    </Grid>
  );
}

const Statement = (props) => {
  return (
    <div>
      <Typography variant="h6">
        Statement
      </Typography>
      <HTMLEditor
        darkTheme
        height={400}
        initialValue={props.statement}
        onChange={props.handleEditorStatementChange}
      />
    </div>
  );
}

const Input = (props) => {
  return (
    <div>
      <Typography variant="h6">
        Input description
      </Typography>
      <HTMLEditor
        darkTheme
        height={400}
        initialValue={props.inputDescription}
        onChange={props.handleEditorInputChange}
      />
    </div>
  );
}

const Output = (props) => {
  return (
    <div>
      <Typography variant="h6">
        Output description
      </Typography>
      <HTMLEditor
        darkTheme
        height={400}
        initialValue={props.outputDescription}
        onChange={props.handleEditorOutputChange}
      />
    </div>
  );
}

const AuthorAndHints = (props) => {
  const classes = useStyles();
  return (
    <Grid container>
      <Grid item md={3} className={classes.textFields}>
        <TextField id="author" name="author" label="Author" variant="outlined" value={props.author} onChange={props.handleChange} fullWidth />
      </Grid>
      <Grid item md={9} className={classes.textFields}>
        <TextField
          id="hints"
          name="hints"
          label="Hints"
          multiline
          rows={12}
          rowsMax={50}
          fullWidth
          onChange={props.handleChange}
          value={props.hints}
        />
      </Grid>
    </Grid>
  );
}


const CreateProblem = ({ history }) => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [completed, setCompleted] = React.useState({});
  const auth = useContext(AuthContext);
  const { sendRequest, isLoading, error, clearError } = useHttpClient();

  const initialProblemState = {
    title: '',
    timeLimit: 1,
    memoryLimit: 1,
    statement: '',
    inputDescription: '',
    outputDescription: '',
    inputExample: '',
    outputExample: '',
    author: '',
    hints: ''
  };
  const [problem, setProblem] = useState(initialProblemState);

  const steps = getSteps();

  const handleChange = (event) => {
    const field = event.target.name;
    const value = event.target.value;
    setProblem(oldState => ({ ...oldState, [field]: value }));
  }

  const handleEditorStatementChange = (content, editor) => {
    setProblem(oldState => ({ ...oldState, statement: content }));
  }

  const handleEditorInputChange = (content, editor) => {
    setProblem(oldState => ({ ...oldState, inputDescription: content }));
  }

  const handleEditorOutputChange = (content, editor) => {
    setProblem(oldState => ({ ...oldState, outputDescription: content }));
  }

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const getStepContent = step => {
    switch (step) {
      case 0:
        return <TitleAndLimits title={problem.title} memoryLimit={problem.memoryLimit} timeLimit={problem.timeLimit} handleChange={handleChange} />;
      case 1:
        return <Statement statement={problem.statement} handleEditorStatementChange={handleEditorStatementChange} />;
      case 2:
        return <Input inputDescription={problem.inputDescription} handleEditorInputChange={handleEditorInputChange} />;
      case 3:
        return (
            <TextField
              id="inputExample"
              name="inputExample"
              label="Input example"
              multiline
              rows={12}
              rowsMax={50}
              fullWidth
              value={problem.inputExample}
              onChange={handleChange}
            />
        );
      case 4:
        return <Output outputDescription={problem.outputDescription} handleEditorOutputChange={handleEditorOutputChange} />;
      case 5:
        return (
            <TextField
              id="outputExample"
              name="outputExample"
              label="Output example"
              multiline
              rows={12}
              value={problem.outputExample}
              rowsMax={50}
              onChange={handleChange}
              fullWidth
            />
        );
      case 6:
        return <AuthorAndHints author={problem.author} hints={problem.hints} handleChange={handleChange}/>;
      default:
        return null;
    }
  }

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
    setCompleted({});
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };

  const totalSteps = () => {
    return steps.length;
  };

  const completedSteps = () => {
    return Object.keys(completed).length;
  };

  const handleComplete = () => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;
    setCompleted(newCompleted);
    handleNext();
  };

  const handleSubmit = async () => {
    const userId = auth.user.id;
    const body = { ...problem, userId };
    const {data: problemCreated} = await sendRequest('/problems', 'POST', body);
    history.push('/problems/show/' + problemCreated.id);
  }

  return (
    <Container className={classes.root}>
      <MuiThemeProvider theme={theme}>
        <Typography variant="h4" className={classes.title}>
          Create Problem
        </Typography>
        <Stepper nonLinear activeStep={activeStep} alternativeLabel>
          {steps.map((label, index) => (
            <Step key={label}>
              <StepButton onClick={handleStep(index)} completed={completed[index]}>
                {label}
              </StepButton>
            </Step>
          ))}
        </Stepper>
        {allStepsCompleted() ? (
          <div>
            <Typography className={classes.instructions}>
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset} className={classes.button}>Reset</Button>
            <ColorButton
              color="green"
              contrast={700}
              variant="contained"
              endIcon={<Send />}
              component={Link}
              to="/problems/new"
              className={classes.button}
              onClick={handleSubmit}
            >
              Submit
            </ColorButton>
          </div>
        ) : (
          <div>
            { getStepContent(activeStep) }
            <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
              Back
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={handleNext}
              className={classes.button}
            >
              Next
            </Button>
            {activeStep !== steps.length &&
              (completed[activeStep] ? (
                <Typography variant="caption" className={classes.completed}>
                  Step {activeStep + 1} already completed
                </Typography>
              ) : (
                <Button variant="contained" color="primary" onClick={handleComplete} className={classes.button}>
                  {completedSteps() === totalSteps() - 1 ? 'Finish' : 'Complete Step'}
                </Button>
              ))}
          </div>
        )}
      </MuiThemeProvider>
    </Container>
  );
}

export default CreateProblem;
