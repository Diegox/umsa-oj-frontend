import React, { useState, useEffect } from 'react'

import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider, Container, CircularProgress, Typography, FormControlLabel, TextField } from "@material-ui/core";

// import api from '../../api/AxiosClient';
import { tableDark } from '../../constants/themes';
import { useHttpClient } from '../../hooks/httpHook';

export default function ListProblems ({ history }) {

  const { sendRequest, error, clearError, isLoading } = useHttpClient();
  let theme = createMuiTheme(tableDark);

  const initialState = {
    page: 0,
    rowsPerPage: 20,
    searchText: '',
    sortOrder: {
      name: '',
      direction: ''
    },
    count: 0,
    data: [],
    columns: [
      {
        name: "id",
        label: "ID",
        options: {
          customBodyRender: (value, tableMeta, updateValue) => {

            // Here you can render a more complex display.
            // You're given access to tableMeta, which has
            // the rowData (as well as the original object data).
            // See the console for a detailed look at this object.

            // console.log('value:', value);

            return (
              // <Typography component={Link} to={`/problems/${value}`} style={{textDecoration: 'none', color: theme.palette.text.primary}}>
                value
              // </Typography>
            );
          }
        },
      },
      {
        name: "title",
        label: "Title",
        options: {
          customBodyRender: (value, tableMeta, updateValue) => {
            return (value);
          }
        },
      },
      {
        name: "author",
        label: "Author",
        options: {},
      },
      {
        name: "accepted",
        label: "Accepted",
        options: {
          customBodyRender: (value, tableMeta, updateValue) => (Math.floor(Math.random() * 10))
        },
      },
      {
        name: "sent",
        label: "Sent",
        options: {
          customBodyRender: (value, tableMeta, updateValue) => (Math.floor(Math.random() * 10) + 10)
        },
      },
      {
        name: "accuracy",
        label: "Acccuracy",
        options: {
          customBodyRender: (value, tableMeta, updateValue) => ('-')

        },
      }
    ]
  };

  const [state, setState] = useState(initialState);
  const { page, rowsPerPage, sortOrder, count, data, searchText } = state;

  const options = {
    serverSide: true,
    filter: false,
    download: false,
    caseSensitive: false,
    pagination: true,
    jumpToPage: true,
    print: false,
    // selectableRowsOnClick: true,
    selectableRows: 'none',
    // filterType: 'dropdown',
    responsive: 'standard',
    // setRowProps: (row, dataIndex, rowIndex) => {
    //   return ({
    //     style: { background: rowIndex % 7 == 0 ? theme.palette.error.dark: rowIndex % 3 === 0 ? theme.palette.success.dark : null }
    //   });
    // },
    count,
    rowsPerPage,
    rowsPerPageOptions: [20, 50, 100],
    // sortOrder,
    searchPlaceholder: 'ID, Title, Author',
    // onTablechange: (action, tableState) => {
    //   console.log('action, tableState:', action, tableState);
    //   switch (action) {
    //     case 'filter':
    //       this.filter(tableState.page, tableState.sortOrder);
    //     case 'changePage':
    //       this.changePage(tableState.page, tableState.sortOrder);
    //       break;
    //     case 'sort':
    //       this.sort(tableState.page, tableState.sortOrder);
    //     default:
    //       console.log('---- Action not enabled ----');
    //   }
    // },
    onChangePage: (currentPage) => {
      console.log('currentPage:', currentPage);
      getProblems(currentPage, sortOrder, rowsPerPage, searchText);
    },
    onColumnSortChange: (changedColumn, direction) => {
      console.log('changedColumn, direction:', changedColumn, direction);
      const sortOpts = {
        name: changedColumn,
        direction
      }
      getProblems(page, sortOpts, rowsPerPage, searchText);
    },
    onSearchChange: (text) => {
      console.log('searchText:', text);
      getProblems(page, sortOrder, rowsPerPage, text);
    },
    onChangeRowsPerPage: (numberOfRows) => {
      console.log('numberOfRows:', numberOfRows);
      getProblems(page, sortOrder, numberOfRows, searchText);
    },
    onRowClick: (rowData) => {
      console.log('rowData:', rowData);
      history.push('/problems/show/' + rowData[0]);
    }
  };

  useEffect(() => {
    getProblems(page, sortOrder, rowsPerPage, searchText);
  }, []);

  const getProblems = async (page, sortOrder, rowsPerPage, searchText) => {
    const query = `?page=${page}&rowsPerPage=${rowsPerPage}&searchText=${searchText}&sortName=${sortOrder.name}&sortDirection=${sortOrder.direction}`;
    const { data: problems } = await sendRequest('/problems' + query);
    setState(prevState => ({
      ...prevState,
      data: problems.data,
      count: problems.count,
      sortOrder,
      rowsPerPage
    }));
  }

  return (
    <Container>
      <MuiThemeProvider theme={theme}>
        <MUIDataTable
          title={<h1 style={{ borderBottom: '2px solid #FDD835', display: 'inline' }}>Problems</h1>}
          // title={
          //   <Typography variant="h6">
          //     Problems
          //   {isLoading && <CircularProgress size={24} style={{marginLeft: 15, position: 'relative', top: 4}} />}
          //   </Typography>
          // }
          data={data}
          columns={state.columns}
          options={options}
        />
      </MuiThemeProvider>
    </Container>
  );
}
