import React, { Fragment, useContext, useEffect, useState } from 'react';
import Editor, { useMonaco, loader, DiffEditor } from '@monaco-editor/react';
import { Container, FormControl, InputLabel, makeStyles, MenuItem, Select, Typography, Button, createMuiTheme, MuiThemeProvider, Grid } from '@material-ui/core';
import { useHistory, useParams } from 'react-router';
import { DropzoneArea, DropzoneDialog } from 'material-ui-dropzone';
import { useHttpClient } from '../../hooks/httpHook';
import { tableDark } from '../../constants/themes';
import { Send } from '@material-ui/icons';
import { AuthContext } from '../../context/authContext';
import { JUDGER_SERVICE } from '../../constants/services';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  title: {
    borderBottom: '1px solid #FDD835'
  }
}));

const SubmitSolution = (props) => {
  const history = useHistory();
  const { id: problemId} = useParams();
  const classes = useStyles();
  const [problem, setProblem] = useState(null);
  const [language, setLanguage] = useState('');
  const [solutionSourceCode, setSolutionSourceCode] = useState(null);
  const [openUploadSulutionDialog, setOpenUploadSolutionDialog] = useState(false);
  const auth = useContext(AuthContext);
  const { sendRequest, isLoading, clearError, error } = useHttpClient();
  let theme = createMuiTheme(tableDark);

  const handleEditorChange = (value, event) => {
    console.log(value);
  }

  const handleChangeLanguage = (event) => {
    console.log('event.target.value: ', event.target.value);
    setLanguage(event.target.value);
  }

  useEffect(() => {
    const fetchProblem = async () => {
      const { data: problem } = await sendRequest(`/problems/${problemId}`);
      setProblem(problem);
    }
    fetchProblem();
  }, []);

  const handleSaveFiles = (files) => {
    const file = files[0];
    setSolutionSourceCode(file);
  }

  const handleSubmit = () => {
    const body = {
      problemId: problem.id,
      language: language,
      userId: auth.user.id
    }
    const data = new FormData();
    for (const key in body) {
      data.append(key, body[key]);
    }
    data.append('solution', solutionSourceCode);
    sendRequest('/judger/run', 'POST', data, JUDGER_SERVICE);
    // history.push('/status');
  }

  return (
    <Container>
      <Grid container direction="column" spacing={2}>
        <Grid item sm>
          <Typography variant="h4">Submit Solution</Typography>
        </Grid>
        <Grid item sm={12} md={3}>
          <Typography variant="h3" className={classes.title}>{problem ? problem.title : ''}</Typography>
        </Grid>
        <Grid item>
          <Typography variant="body2">Select a language:</Typography>
          <MuiThemeProvider theme={theme}>
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">Language</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={language}
                onChange={handleChangeLanguage}
              >
                <MenuItem value={'java'}>Java</MenuItem>
                <MenuItem value={'cpp'}>C++</MenuItem>
                <MenuItem value={'python'}>Python</MenuItem>
                <MenuItem value={'javascript'}>Javascript</MenuItem>

              </Select>
            </FormControl>
          </MuiThemeProvider>
        </Grid>
        <Grid item>
        <Typography variant="body2">Source Code:</Typography>
        <Editor
          height="50vh"
          defaultLanguage={language}
          language={language}
          defaultValue=""
          onChange={handleEditorChange}
          theme="vs-dark"
        />
        </Grid>
        <Grid item>
          <Typography variant="body2">Or choose a file:</Typography>
          <Grid item container direction="row" spacing={2}>
            <Grid item>
              <Button variant="contained" color="primary" onClick={() => setOpenUploadSolutionDialog(true)}>
                Upload Sulution
              </Button>
            </Grid>
            <Grid item>
              {solutionSourceCode ? <Typography variant="body2">{solutionSourceCode.name}</Typography> : <Typography variant="body2">No file chosen</Typography>}
            </Grid>
          </Grid>
        </Grid>
        <MuiThemeProvider theme={theme}>
          <DropzoneDialog
            cancelButtonText={"cancel"}
            submitButtonText={"ok"}
            maxFileSize={5000000}
            open={openUploadSulutionDialog}
            onClose={() => setOpenUploadSolutionDialog(false)}
            onSave={(files) => {
              handleSaveFiles(files);
              setOpenUploadSolutionDialog(false);
            }}
            showPreviews={true}
            showFileNamesInPreview={true}
          />
        </MuiThemeProvider>
        <Grid item align="center">
            <Button variant="contained" color="secondary" endIcon={<Send />} onClick={handleSubmit}>Submit</Button>
        </Grid>
      </Grid>
    </Container>
  );
}

export default SubmitSolution;
