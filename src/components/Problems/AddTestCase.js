import { Button, Container, createMuiTheme, Dialog, FormControl, FormControlLabel, FormLabel, Grid, makeStyles, MuiThemeProvider, Radio, RadioGroup, TextField, Typography, withStyles } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { Add, CloudUpload } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import { tableDark } from '../../constants/themes';
import MUIDataTable from 'mui-datatables';
import { DropzoneArea, DropzoneDialog } from 'material-ui-dropzone';
import { useHistory, useParams } from 'react-router';
import { useHttpClient } from '../../hooks/httpHook';
import { JUDGER_SERVICE } from '../../constants/services';

const useStyles = makeStyles(theme => ({
  italic: {
    fontStyle: 'italic'
  },
  gridItem: {
    margin: theme.spacing(1)
  }
}));

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  }
});

let theme = createMuiTheme(tableDark);

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const AddTestCase = (props) => {
  const classes = useStyles();

  const problemId = useParams().id;
  const history = useHistory();

  const [open, setOpen] = useState(false);
  const [rbInput, setRbInput] = useState('editor');
  const [rbOutput, setRbOutput] = useState('editor');
  const [openUploadDialog, setOpenUploadDialog] = useState(false);
  const [problem, setProblem] = useState(undefined);
  const [testCases, setTestCases] = useState([]);
  const { sendRequest, isLoading, clearError, error } = useHttpClient();


  const tableColumns = [
    {
      name: 'tag',
      label: 'Tag',
      options: {
        hint: 'Tag your test cases to identify them among other test cases.'
      }
    },
    {
      name: 'inputFileName',
      label: 'Input',
      options: {
        hint: 'Expected output testcase, will be compared with STDOUT of a submission.'
      }
    },
    {
      name: 'outputFileName',
      label: 'Output',
      options: {
        hint: 'Expected output testcase, will be compared with STDOUT of a submission.'
      }
    },
    {
      name: 'judge',
      label: 'Judge',
      options: {
        hint: 'Select to find out how much test cases contribute to the max score.'
      }
    },
    {
      name: 'level',
      label: 'Level',
      options: {
        hint: 'Select to find out how much test cases contribute to the max score.'
      }
    },

    {
      name: 'actions',
      label: 'Actions',
      options: {
        hint: 'Expected output testcase, will be compared with STDOUT of a submission.'
      }
    }
  ];
  const tableOptions = {
    resizableColumns: false,
    sort: false,
    serverSide: true,
    filter: false,
    download: false,
    caseSensitive: false,
    pagination: false,
    jumpToPage: false,
    print: false,
    search: false,
    selectableRows: 'none',
    responsive: 'standard',
    textLabels: {
      body: {
        noMatch: 'No test cases have been added yet'
      }
    }
  };


  useEffect(() => {

    const fetchProblemWithTestCases = async () => {
      const { data: problemWithTestCases } = await sendRequest(`/problems/${problemId}/test-cases`);
      const { testCases, ...problem } = problemWithTestCases;
      console.log('testCases: ', testCases);
      console.log('problem: ', problem);
      setProblem(problem);
      setTestCases(testCases);
    }
    fetchProblemWithTestCases();
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  }

  const handleClose = () => {
    setOpen(false);
  }

  const handleRbInputChange = (event) => {
    setRbInput(event.target.value);
  };

  const handleRbOutputChange = (event) => {
    setRbOutput(event.target.value);
  };

  const handleUploadZip = (files) => {
    console.log('files:', files);
    console.log('problem: ', problem);
    const body = {
      problemId: problem.id,
      problemName: problem.title
    }
    const data = new FormData();
    for (const key in body) {
      data.append(key, body[key]);
    }
    const file = files[0];
    data.append('testCases', file);
    sendRequest('/test-cases/upload-zip', 'POST', data, JUDGER_SERVICE);
    setOpenUploadDialog(false);
  }

  return (
    <Container>
      <Grid container direction="column" spacing={2} >
        <Grid item>
          <Typography variant="h3">
            { problem ? problem.title : '' }
          </Typography>
        </Grid>
        <Grid item>
        <Typography variant="body2" className={classes.italic}>
          Add test cases to judge the correctness of a user’s code. Refer to these instructions to write a good test case.
        </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body2" className={classes.italic}>
            Download sample test cases from Hello World challenge to understand the .zip format.
          </Typography>
        </Grid>
        <Grid item container spacing={2} direction="row">
          <Grid item>
            <Button  variant="outlined" startIcon={ <CloudUpload /> } onClick={() => setOpenUploadDialog(true)}>
              Upload Zip
            </Button>
          </Grid>
          <Grid item>
            <Button color="secondary" variant="contained" startIcon={ <Add /> } onClick={handleClickOpen}>
              Add Test Case
            </Button>
          </Grid>
        </Grid>
        <Grid item>
          <MuiThemeProvider theme={theme}>
            <MUIDataTable
              title="Test Cases"
              data={testCases}
              options={tableOptions}
              columns={tableColumns}
            />
          </MuiThemeProvider>
        </Grid>
      </Grid>
      <MuiThemeProvider theme={theme}>
        <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open} fullWidth maxWidth='md'>
          <DialogTitle id="customized-dialog-title" onClose={handleClose}>
            Add Test Case
          </DialogTitle>
          <DialogContent>
            <Grid container direction="column" justify="space-between" alignItems="flex-start">

              <TextField id="tag" label="Tag" className={classes.gridItem} />

              <FormControl component="fieldset" className={classes.gridItem}>
                <FormLabel component="legend">Input</FormLabel>
                <RadioGroup row aria-label="input" name="input1" value={rbInput} onChange={handleRbInputChange}>
                  <FormControlLabel value="editor" control={<Radio color="primary"/>} label="Editor" />
                  <FormControlLabel value="upload" control={<Radio color="primary" />} label="Upload" />
                </RadioGroup>
              </FormControl>
              {
                rbInput === 'editor'
                ?
                <TextField fullWidth variant="filled" id="input" label="Input" multiline helperText="Size: 0 Kb" className={classes.gridItem}/>
                :
                <DropzoneArea
                  filesLimit={1}
                  showPreviews={true}
                  showPreviewsInDropzone={false}
                  useChipsForPreview
                  previewGridProps={{container: { spacing: 1, direction: 'row' }}}
                  previewChipProps={{classes: { root: classes.previewChip } }}
                  previewText="Selected file"
                  acceptedFiles={['text/plain']}
                />
              }


              <FormControl component="fieldset" className={classes.gridItem}>
                <FormLabel component="legend">Output</FormLabel>
                <RadioGroup row aria-label="output" name="output1" value={rbOutput} onChange={handleRbOutputChange}>
                  <FormControlLabel value="editor" control={<Radio color="primary"/>} label="Editor" />
                  <FormControlLabel value="upload" control={<Radio color="primary" />} label="Upload" />
                </RadioGroup>
              </FormControl>
              {
                rbOutput === 'editor'
                ?
                <TextField fullWidth variant="filled" id="output" label="Output" multiline helperText="Size: 0 Kb" className={classes.gridItem}/>
                :
                <DropzoneArea
                  filesLimit={1}
                  showPreviews={true}
                  showPreviewsInDropzone={false}
                  useChipsForPreview
                  previewGridProps={{container: { spacing: 1, direction: 'row' }}}
                  previewChipProps={{classes: { root: classes.previewChip } }}
                  previewText="Selected file"
                  acceptedFiles={['text/plain']}
                />
              }

            </Grid>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleClose} color="primary">
              Save changes
            </Button>
          </DialogActions>
        </Dialog>
        <DropzoneDialog
          children={ <h1>asd</h1> }
          dialogTitle="Upload Zip"
          acceptedFiles={['application/zip']}
          cancelButtonText={"cancel"}
          submitButtonText={"submit"}
          maxFileSize={5000000}
          open={openUploadDialog}
          onClose={() => setOpenUploadDialog(false)}
          onSave={(files) => {
            handleUploadZip(files);
          }}
          showPreviews={true}
          showFileNamesInPreview={true}
        />
      </MuiThemeProvider>

    </Container>
  );
}

export default AddTestCase;
