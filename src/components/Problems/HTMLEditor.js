import React from 'react';

import { Editor } from '@tinymce/tinymce-react';

const API_KEY = 'gwe7epqc32dbsv229uq80ren4e22fafr6i9n4jw23tgj6rs5';

//TODO: support latex

const HTMLEditor = (props) => {
  const { darkTheme, height, onChange, initialValue } = props;
  let editorSettings = {
    height,
    menubar: true,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code codesample fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar:
      'undo redo | formatselect | bold italic backcolor | \
      alignleft aligncenter alignright alignjustify | \
      bullist numlist outdent indent codesample | removeformat | help '
  }

  if (darkTheme) {
    editorSettings.skin = 'oxide-dark';
    editorSettings.content_css = 'dark'
  }

  const handleChange = (content, editor) => {
    onChange(content, editor);
  }

  return (
    <div>
      <Editor
        apiKey={API_KEY}
        initialValue={initialValue}
        init={editorSettings}
        onEditorChange={handleChange}
      />
    </div>
  );
}

export default HTMLEditor;
