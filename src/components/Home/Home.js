import React, { useState, Fragment } from 'react';
import { Button, Container, makeStyles, Paper, Typography } from '@material-ui/core';
import Carousel from 'react-material-ui-carousel';
import icpcImage from '../../images/icpc.jpg';
import icpcLogo from '../../images/icpc_logo.png';
import obiImage from '../../images/obi.jpg';
import HomeCard from './HomeCard';

const useStyles = makeStyles(theme => ({
  carousel: {

  }
}));

const Item= (props) =>{
  const { item } = props;
  return (
    <HomeCard title={item.title} description={item.description} img={item.img} link={item.link} />
  )
}

export default function Home() {
  const classes = useStyles();

  const items = [
    {
      title: "ICPC",
      img: 'icpc',
      description: 'The International Collegiate Programming Contest is an algorithmic programming contest for college students. Teams of three, representing their university, work to solve the most real-world problems, fostering collaboration, creativity, innovation, and the ability to perform under pressure. Through training and competition, teams challenge each other to raise the bar on the possible. Quite simply, it is the oldest, largest, and most prestigious programming contest in the world.',
      link: 'https://icpc.global/regionals/abouticpc'
    },
    {
      title: "OBI",
      img: 'obi',
      description: 'La IOI (International Olympiad in Informatics) es una de las competencias en Ciencias de la Computación mas reconocidas a nivel mundial. Es de naturaleza algorítmica, donde el competidor debe desarrollar habilidades básicas en análisis de problemas, diseño de algoritmos y estructura de datos, además del proceso de programación y testeo.',
      link: 'https://olimpiada.icpc-bolivia.edu.bo/'
    },
  ];
  return (
    <Fragment>
      <Container>
        <Carousel
          className={classes.carousel}
          animation="fade"
        >
            {
                items.map((item, i) => <Item key={i} item={item} /> )
            }
        </Carousel>
      </Container>
    </Fragment>
  );
}
