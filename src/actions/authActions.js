import types from '../constants/actionTypes/authTypes';

export const loginStart = credentials => ({
  type: types.LOG_IN_START,
  payload: credentials
});

export const loginSuccess = user => ({
  type: types.LOG_IN_SUCCESS,
  payload: user
});

export const loginFailure = error => ({
  type: types.LOG_IN_FAILURE,
  payload: error
});

export const registerStart = credentials => ({
  type: types.REGISTER_START,
  payload: credentials
});

export const registerSuccess = user => ({
  type: types.REGISTER_SUCCESS,
  payload: user,
});

export const registerFailure = error => ({
  type: types.REGISTER_FAILURE,
  payload: error,
});

export const logOut = () => ({
  type: types.LOG_OUT,
});