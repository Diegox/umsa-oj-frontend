import types from '../constants/actionTypes/problemsTypes';

export const getProblemsStart = options => ({
  type: types.GET_PROBLEMS_START,
  payload: options
});

export const getProblemsSuccess = problems => ({
  type: types.GET_PROBLEMS_SUCCESS,
  payload: problems
});

export const getProblemsFailure = error => ({
  type: types.GET_PROBLEMS_START,
  payload: error
});
