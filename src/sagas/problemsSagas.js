import api from '../api/api';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import {
  getProblemsSuccess,
  getProblemsFailure
} from '../actions/problemsActions';

import types from '../constants/actionTypes/problemsTypes';

const getProblems = async options => {
  const response = await api.instance.post('/problems', options);
  return response.data.data;
}

export function* getProblems({ payload }) {
  try {
    const response = yield login(payload);
    yield put(loginSuccess(response));
  } catch (error) {
    yield put(loginFailure(error));
  }
}

export function* onGetProblemsStart() {
  yield takeLatest(types.GET_PROBLEMS_START, getProblems);
}

export function* problemsSagas() {
  yield all([
    call(onGetProblemsStart)
  ]);
}