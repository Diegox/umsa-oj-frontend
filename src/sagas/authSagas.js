// import api from '../api/AxiosClient';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import {
  loginSuccess,
  loginFailure,
  registerFailure,
  registerSuccess
} from '../actions/authActions';

import types from '../constants/actionTypes/authTypes';

const register = async credentials => {
  // const response = await api.instance.post('/signup', credentials.payload);
  // return response;
};

const login = async credentials => {
  // const response = await api.instance.post('/login', credentials);
  // return response.data.data;
}

export function* registerWithCredentials(credentials) {
  try {
    const response = yield register(credentials);
    yield put(registerSuccess(response));
  } catch (error) {
    yield put(registerFailure(error));
  }
}

export function* loginWithCredentials({ payload }) {
  try {
    const response = yield login(payload);
    yield put(loginSuccess(response));
  } catch (error) {
    yield put(loginFailure(error));
  }
}

export function* onRegisterStart() {
  yield takeLatest(types.REGISTER_START, registerWithCredentials);
}

export function* onLoginStart() {
  yield takeLatest(types.LOG_IN_START, loginWithCredentials);
}

export function* authSagas() {
  yield all([
    call(onRegisterStart),
    call(onLoginStart)
  ]);
}
